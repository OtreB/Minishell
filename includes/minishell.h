/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/08 13:20:56 by cumberto          #+#    #+#             */
/*   Updated: 2017/09/14 18:09:54 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_H
# define MINISHELL_H

# include <unistd.h>
# include <stdlib.h>
# include <sys/stat.h>
# include <sys/types.h>
# include <sys/wait.h>
# include <fcntl.h>
# include <dirent.h>
# include "../libft/includes/libft.h"
# include <stdbool.h>
# define SIZE 1024
# define EXE_NAME "minishell"
# define PATH_SIZE 256

int			env(char **arg);
int			ft_execute(char **args, char **env);
int			cd(char *str);
int			env_mod(char **arg);
char		*set_env(char *var, char *name);
int			unset_env(char *name);
int			error_msg_m(int err, char *arg, char *proc_name);
void		set_key_env();
void		unset_key_env();
int			chr_interpreter(char chr);
char		*read_cmd_line();
char		*find_pattern(char *pattern);
char		*fix_path(char *path);
char		**new_env();
void		free_table(char **table);
int			ft_echo(char **args);
char		*path_manager(char *cmd);
char		*fix_path(char *path);
int			env_name_cmp(char *env, char *name);

pid_t g_pid;
int		g_ret;

# define RED   "\x1B[31m"
# define GRN   "\x1B[32m"
# define YEL   "\x1B[33m"
# define BLU   "\x1B[34m"
# define MAG   "\x1B[35m"
# define CYN   "\x1B[36m"
# define WHT   "\x1B[37m"
# define RESET "\x1B[0m"

#endif
