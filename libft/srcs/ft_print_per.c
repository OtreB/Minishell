/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_ft_print_i.c.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/13 15:12:34 by cumberto          #+#    #+#             */
/*   Updated: 2017/09/08 12:34:18 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		ft_print_per(t_print *elem)
{
	char *dst;

	dst = NULL;
	if (elem->width > 1)
	{
		print_str(elem, "%", 1);
		return (elem->width);
	}
	else
		write(g_fd, "%", 1);
	return (1);
}
