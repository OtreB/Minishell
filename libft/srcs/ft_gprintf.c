/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_1.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/02 11:22:56 by cumberto          #+#    #+#             */
/*   Updated: 2017/09/08 13:34:44 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		check_format(const char *str, t_print elem)
{
	int cnt;

	cnt = 1;
	if (str[cnt] == '\0')
		return (0);
	while (str[cnt] != elem.spec)
		cnt++;
	return (cnt);
}

int		ft_gprintf(int fd, const char *format, va_list *arg)
{
	t_print	elem;
	int		i;
	int		num;

	g_fd = fd;
	num = 0;
	i = 0;
	while (format[i] != '\0')
	{
		if (format[i] != '%')
			ft_putchar_fd(format[i], g_fd);
		else
		{
			ft_bzero(&elem, sizeof(t_print));
			read_format(&format[i + 1], &elem, arg);
			if ((num += print_arg(&elem, arg) - 1) < -1)
				return (-1);
			i += check_format(&format[i], elem);
		}
		i++;
		num++;
	}
	return (num);
}
