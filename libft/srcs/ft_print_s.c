/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_s.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/06 18:07:26 by cumberto          #+#    #+#             */
/*   Updated: 2017/05/12 14:44:13 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		wchar_size_p(wchar_t chr)
{
	if (chr < 0x7f)
		return (1);
	else if (chr < 0x7ff)
		return (2);
	else if (chr < 0xffff)
		return (3);
	else
		return (4);
}

int		wchar_str_size(wchar_t *str, t_print *elem)
{
	int		size;
	int		len;
	int		cnt;

	cnt = 0;
	size = 0;
	while (str[cnt])
	{
		len = wchar_size_p(str[cnt]);
		if ((size += len) > elem->prec)
		{
			size -= len;
			break ;
		}
		cnt++;
	}
	return (size);
}

int		print_wchar_str(t_print *elem, wchar_t *str)
{
	int		size;
	char	*dst;

	if (str == NULL)
		dst = ("(null)");
	else if (elem->prec == 0)
		dst = "";
	else
		dst = ft_conv_wchar(str);
	size = ft_strlen(dst);
	if (elem->prec < size && elem->prec > 0)
		size = wchar_str_size(str, elem);
	print_str(elem, dst, size);
	if (elem->width > size)
		size = elem->width;
	if (str != NULL && elem->prec != 0)
		free(dst);
	return (size);
}

int		ft_print_str(t_print *elem, va_list arg)
{
	char	*str;
	int		size;

	if (elem->spec == 'S' || elem->mod[0] == 'l')
		return (print_wchar_str(elem, va_arg(arg, wchar_t*)));
	else
	{
		str = va_arg(arg, char*);
		if (str == NULL)
			str = ("(null)");
		if (elem->prec == 0)
			str = "";
		size = ft_strlen(str);
		if (size > elem->prec && elem->prec > 0)
			size = elem->prec;
		print_str(elem, str, size);
		if (elem->width > size)
			size = elem->width;
	}
	return (size);
}
