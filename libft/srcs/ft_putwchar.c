/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putwchar.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/22 15:12:57 by cumberto          #+#    #+#             */
/*   Updated: 2017/09/08 14:01:07 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putwchar(int fd, wchar_t chr)
{
	if (chr < 0x7f)
		ft_putchar_fd(chr, fd);
	else if (chr < 0x7ff)
	{
		ft_putchar_fd((chr >> 6) | 0xc0, fd);
		ft_putchar_fd((chr | 0x80) & 0xbf, fd);
	}
	else if (chr < 0xffff)
	{
		ft_putchar_fd((chr >> 12) | 0xe0, fd);
		ft_putchar_fd(((chr >> 6) | 0x80) & 0xbf, fd);
		ft_putchar_fd((chr | 0x80) & 0xbf, fd);
	}
	else
	{
		ft_putchar_fd(((chr >> 18) | 0xf0) & 0xf7, fd);
		ft_putchar_fd(((chr >> 12) | 0x80) & 0xbf, fd);
		ft_putchar_fd(((chr >> 6) | 0x80) & 0xbf, fd);
		ft_putchar_fd((chr | 0x80) & 0xbf, fd);
	}
}
