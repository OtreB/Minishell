/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_x.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/06 17:42:26 by cumberto          #+#    #+#             */
/*   Updated: 2017/05/12 14:44:17 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	print_str_und(t_print *elem, char *str, int size, int max)
{
	if (elem->width > max)
	{
		if (elem->flag_m == 1)
		{
			if (elem->prec > size)
				print_char('0', elem->prec - size);
			write(1, str, size);
			print_char(elem->chr, elem->width - max);
			return ;
		}
		else
			print_char(elem->chr, elem->width - max);
	}
	if (elem->prec > size)
		print_char('0', elem->prec - size);
	write(1, str, size);
}

int		ft_print_und(t_print *elem, va_list *arg)
{
	char	*str;
	int		size;
	int		max;

	if (elem->prec == 0)
		return (0);
	str = ft_itoa_unsign(manage_unsigned_mod(elem, *arg));
	size = ft_strlen(str);
	max = elem->prec > size ? elem->prec : size;
	print_str_und(elem, str, size, max);
	free(str);
	if (elem->width > max)
		max = elem->width;
	return (max);
}
