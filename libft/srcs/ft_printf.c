/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/08 12:43:14 by cumberto          #+#    #+#             */
/*   Updated: 2017/09/08 13:14:26 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_printf(const char *format, ...)
{
	va_list	arg;
	int		ret;

	va_start(arg, format);
	ret = ft_gprintf(1, format, &arg);
	va_end(arg);
	return (ret);
}
