# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: cumberto <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/07/08 13:11:13 by cumberto          #+#    #+#              #
#    Updated: 2017/09/14 17:40:01 by cumberto         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

SRC_NAME =	main.c \
			error_m.c \
			key_rec.c \
			cd.c \
			echo.c \
			env.c \
			setenv.c \
			env_attr.c \
			exec.c \
			free.c \
			path_fix.c \

SRC_PATH = srcs

SRC = $(addprefix $(SRC_PATH)/,$(SRC_NAME))

OBJ_NAME = $(SRC_NAME:.c=.o)

OBJ_PATH = obj

OBJ = $(addprefix $(OBJ_PATH)/,$(OBJ_NAME))

INCLUDES = -Iincludes

NAME = minishell

CFLAGS = -Wall -Werror -Wextra

all: $(NAME)

$(NAME): libft/libft.a $(OBJ)
	gcc $(INCLUDES) $(CFLAGS) $(OBJ) libft/libft.a -o $(NAME)

libft/libft.a:
	make -C ./libft

$(OBJ_PATH)/%.o:$(SRC_PATH)/%.c
	mkdir -p $(OBJ_PATH)
	gcc $(CFLAGS) $(INCLUDES) -c $< -o $@

clean:
	make -C ./libft clean
	rm -Rf $(OBJ_PATH)

fclean: clean
	make -C ./libft fclean
	rm -f minishell

re: fclean libft/libft.a all

test: libft/libft.a
	gcc -g $(CFLAGS) $(SRC) $(addprefix $(BUILT_IN_PATH)/,$(BUILT_IN_SRC)) $(INCLUDES) libft/libft.a -o $(NAME)
