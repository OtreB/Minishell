/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/08 13:31:22 by cumberto          #+#    #+#             */
/*   Updated: 2017/09/14 17:50:46 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

#define CURSOR_BACK(x) ft_printf("\033[%dD", (x))
#define CURSOR_FORE(x) ft_printf("\033[%dC", (x))

int		arrow(unsigned int *cursor, unsigned int *cnt)
{
	char ch;

	read(0, &ch, 1);
	if (ch == 91)
	{
		read(0, &ch, 1);
		if (ch == 'C')
		{
			if (*cursor < *cnt)
			{
				CURSOR_FORE(1);
				++*cursor;
			}
		}
		else if (ch == 'D')
		{
			if (*cursor > 0)
			{
				CURSOR_BACK(1);
				--*cursor;
			}
		}
	}
	return (0);
}

int		printchar(char ch, char *buf, unsigned int *cursor, unsigned int *cnt)
{
	char *tmp;

	if (*cursor < *cnt)
	{
		tmp = ft_strdup(&buf[*cursor]);
		buf[*cursor] = ch;
		buf[++*cursor] = '\0';
		buf = ft_strcat(buf, tmp);
		ft_printf("%c%s", ch, tmp);
		CURSOR_BACK(ft_strlen(tmp));
		free(tmp);
		++*cnt;
	}
	else
	{
		write(1, &ch, 1);
		++*cursor;
		buf[*cnt] = ch;
		++*cnt;
		return (1);
	}
	return (0);
}

int		deletechar(char *buf, unsigned int *cursor, unsigned int *cnt)
{
	char *tmp;

	if (*cursor == 0)
		return (0);
	if (*cursor < *cnt)
	{
		tmp = ft_strdup(&buf[*cursor]);
		buf[--*cursor] = '\0';
		buf = ft_strcat(buf, tmp);
		--*cnt;
		ft_printf("\b%s ", tmp);
		CURSOR_BACK(ft_strlen(tmp) + 1);
		free(tmp);
	}
	else
	{
		write(1, "\b \b", 3);
		--*cursor;
		--*cnt;
		buf[*cnt] = '\0';
	}
	return (0);
}

int		rec_key(char ch, char *buf, unsigned int *cursor, unsigned int *cnt)
{
	if (ch == '\n')
	{
		CURSOR_FORE(*cnt - *cursor);
		write(1, &ch, 1);
		return (0);
	}
	else if (ch == '\t' || ch == '\f')
		return (0);
	else if (ch == 27)
		return (arrow(cursor, cnt));
	else if (ch == 127)
		return (deletechar(buf, cursor, cnt));
	else
		return (printchar(ch, buf, cursor, cnt));
}

char	*read_cmd_line(void)
{
	int				ch;
	char			*buf;
	unsigned int	cursor;
	unsigned int	cnt;

	ch = 0;
	cursor = 0;
	cnt = 0;
	buf = ft_strnew(SIZE);
	while (ch != '\n')
	{
		read(0, &ch, 1);
		rec_key(ch, buf, &cursor, &cnt);
	}
	return (buf);
}
