/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/08 13:31:22 by cumberto          #+#    #+#             */
/*   Updated: 2017/09/14 17:45:20 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		cmd_interpreter(char *str)
{
	char		**args;
	extern char	**environ;
	int			ret;

	ret = 0;
	args = ft_strsplit(str, ' ');
	if (args[0] != NULL)
	{
		if (ft_strcmp(args[0], "cd") == 0)
			ret = cd(args[1]);
		else if (ft_strcmp(args[0], "exit") == 0)
			ret = -1;
		else if (ft_strcmp(args[0], "env") == 0)
			ret = env(&args[1]);
		else if (ft_strcmp(args[0], "setenv") == 0)
			ret = env_mod(&args[1]);
		else if (ft_strcmp(args[0], "unsetenv") == 0)
			ret = unset_env(args[1]);
		else if (ft_strcmp(args[0], "echo") == 0)
			ret = ft_echo(&args[1]);
		else
			ret = ft_execute(args, environ);
	}
	free_table(args);
	return (ret);
}

void	get_the_name(void)
{
	char			path[SIZE];
	unsigned int	cnt;
	wchar_t			wca;

	wca = L'࿓';
	cnt = 0;
	ft_bzero(path, sizeof(path));
	getcwd(path, SIZE - 1);
	cnt = ft_strlen(path);
	if (cnt == 0)
		return ;
	while (path[cnt] != '/')
		cnt--;
	ft_printf("%s%lc%s  ", (g_ret ? RED : CYN), wca, RESET);
	ft_printf("%s%s%s\x1B[31m>\x1B[0m ", GRN, &path[cnt + 1], RESET);
}

void	parent_handler(int sig)
{
	if (g_pid == 0 && sig == SIGINT)
	{
		g_ret = 1;
		write(1, "\n", 1);
		get_the_name();
	}
}

int		main(int argc, char *argv[])
{
	char		*buf;
	extern char	**environ;

	(void)argv;
	(void)argc;
	environ = new_env();
	signal(SIGINT, parent_handler);
	ft_printf("Welcome to cumberto command interpreter!!!\n\nThis is a basic"
			" shell.\nType \"help\" to get more informations!\n\n");
	set_key_env();
	while (42)
	{
		get_the_name();
		buf = read_cmd_line();
		g_ret = cmd_interpreter(buf);
		free(buf);
		if (g_ret == -1)
			break ;
	}
	free_table(environ);
	unset_key_env();
	return (0);
}
