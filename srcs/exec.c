/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/08 13:31:22 by cumberto          #+#    #+#             */
/*   Updated: 2017/09/14 19:13:15 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int					is_regular(char *path)
{
	struct stat		reg;

	stat(path, &reg);
	return (S_ISDIR(reg.st_mode));
}

char				*select_path(char *cmd)
{
	extern char		**environ;
	unsigned int	cnt;
	char			**path;
	char			*tmp;

	cnt = 0;
	if ((path = ft_strsplit(find_pattern("PATH"), ':')) == NULL)
		return (NULL);
	tmp = ft_strnew(PATH_SIZE);
	while (path[cnt])
	{
		ft_bzero(tmp, sizeof(tmp));
		tmp = ft_strcat(ft_strcat(ft_strcat(tmp, path[cnt]), "/"), cmd);
		if (access(tmp, F_OK) == 0)
			break ;
		cnt++;
	}
	if (path[cnt] == NULL)
	{
		free(tmp);
		tmp = NULL;
	}
	free_table(path);
	return (tmp);
}

char				*check_exec(char *cmd)
{
	char			*path;

	if (ft_strchr(cmd, '/'))
		path = fix_path(cmd);
	else
		path = select_path(cmd);
	if (path == NULL || access(path, F_OK) != 0 || is_regular(path) == 1)
	{
		if (path != NULL)
			free(path);
		error_msg_m(3, cmd, EXE_NAME);
		return (NULL);
	}
	if (access(path, X_OK) != 0)
	{
		free(path);
		error_msg_m(5, cmd, EXE_NAME);
		return (NULL);
	}
	return (path);
}

int					ft_execute(char **args, char **env)
{
	int				status;
	char			*path;

	if ((path = check_exec(args[0])) == NULL)
		return (1);
	if ((g_pid = fork()) < 0)
		return (-1);
	if (g_pid == 0)
		execve(path, args, env);
	else
	{
		waitpid(g_pid, &status, 0);
		g_pid = 0;
		free(path);
		if (WIFSIGNALED(status))
		{
			write(1, "\n", 1);
			WTERMSIG(status);
			return (status);
		}
		if (WIFEXITED(status))
			WEXITSTATUS(status);
		return (status);
	}
	return (1);
}
