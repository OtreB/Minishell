/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error_m.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/14 16:52:25 by cumberto          #+#    #+#             */
/*   Updated: 2017/09/14 16:54:42 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		error_msg_m(int err, char *arg, char *proc_name)
{
	if (err == 1)
		ft_dprintf(2, "%s: no such file or directory: %s\n", proc_name, arg);
	else if (err == 2)
		ft_dprintf(2, "%s: opzione non valida -- \"%s\"\nTry '%s --help' for "
			"more information.\n", proc_name, arg, proc_name);
	else if (err == 3)
		ft_dprintf(2, "%s: command not found: %s\n", proc_name, arg);
	else if (err == 4)
		ft_dprintf(2, "%s: %s: %s not set\n", EXE_NAME, proc_name, arg);
	else if (err == 5)
		ft_dprintf(2, "%s: permission denied: %s\n", proc_name, arg);
	return (err);
}
