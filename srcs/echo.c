/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/08 13:31:22 by cumberto          #+#    #+#             */
/*   Updated: 2017/09/14 16:53:03 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		ft_echo(char **args)
{
	unsigned int	cnt;
	char			*var;

	cnt = 0;
	while (args[cnt])
	{
		if (args[cnt][0] == '$' && args[cnt][1])
		{
			if (args[cnt][1] == '?')
				ft_printf("%d", g_ret);
			else if ((var = find_pattern(&args[cnt][1])) != NULL)
				ft_printf("%s", var);
		}
		else
			ft_printf("%s", args[cnt]);
		cnt++;
		if (args[cnt])
			ft_printf(" ");
	}
	if (cnt > 0)
		write(1, "\n", 1);
	return (0);
}
