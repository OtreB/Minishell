/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   path_fix.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/12 14:17:50 by cumberto          #+#    #+#             */
/*   Updated: 2017/09/14 18:44:24 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

char	*go_back(char *buf)
{
	unsigned int	cnt;

	cnt = ft_strlen(buf);
	while (cnt > 0)
	{
		if (buf[cnt] == '/')
			break ;
		cnt--;
	}
	buf[cnt] = '\0';
	return (buf);
}

char	*fix_path(char *path)
{
	unsigned int	cnt;
	char			*buf;
	char			**args;

	if (path[0] == '/')
		return (ft_strdup(path));
	cnt = 0;
	args = ft_strsplit(path, '/');
	if ((buf = ft_strnew(SIZE)) != NULL)
		if (getcwd(buf, SIZE - 1))
		{
			while (args[cnt])
			{
				if (ft_strcmp(args[cnt], "~") == 0 && find_pattern("HOME"))
					ft_strcpy(buf, find_pattern("HOME"));
				else if (ft_strcmp(args[cnt], "..") == 0)
					buf = go_back(buf);
				else if (ft_strcmp(args[cnt], ".") != 0)
					buf = ft_strcat(ft_strcat(buf, "/"), args[cnt]);
				cnt++;
			}
		}
	free_table(args);
	return (buf);
}
