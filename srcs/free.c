/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/07 15:12:36 by cumberto          #+#    #+#             */
/*   Updated: 2017/09/11 15:27:29 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	free_table(char **table)
{
	unsigned int cnt;

	if (table != NULL)
	{
		cnt = 0;
		while (table[cnt] != NULL)
		{
			free(table[cnt]);
			cnt++;
		}
		free(table);
	}
}
