/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/08 13:31:22 by cumberto          #+#    #+#             */
/*   Updated: 2017/09/14 18:08:41 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		print_env(char **env)
{
	unsigned int ind;

	ind = 0;
	if (env != NULL)
		while (env[ind])
		{
			ft_printf("%s\n", env[ind]);
			ind++;
		}
	return (0);
}

int		env_flag(char *arg, unsigned int *cnt, char ***tmp_env)
{
	if (arg != NULL)
		if (arg[0] == '-')
		{
			if (arg[1] == 'i')
			{
				if (!arg[2])
				{
					*tmp_env = (char**)malloc(sizeof(char*) * 100);
					ft_bzero(*tmp_env, sizeof(*tmp_env));
					*tmp_env[0] = NULL;
					++*cnt;
					return (0);
				}
			}
			return (error_msg_m(2, &arg[2], "env"));
		}
	*tmp_env = new_env();
	return (0);
}

char	**new_env(void)
{
	char			**newenv;
	unsigned int	cnt;
	extern char		**environ;

	cnt = 0;
	newenv = (char**)malloc(sizeof(char*) * 100);
	while (environ[cnt] != NULL)
	{
		newenv[cnt] = ft_strdup(environ[cnt]);
		cnt++;
	}
	newenv[cnt] = NULL;
	return (newenv);
}

void	add_tmp_env(char **env, char *var)
{
	unsigned int	cnt;
	unsigned int	ind;
	char			*buf;

	cnt = 0;
	buf = ft_strdup(var);
	while (env[cnt])
	{
		ind = 0;
		if (env_name_cmp(env[cnt], var) == 0)
		{
			free(env[cnt]);
			env[cnt] = buf;
			return ;
		}
		cnt++;
	}
	env[cnt] = buf;
	cnt++;
	env[cnt] = NULL;
	return ;
}

int		env(char **arg)
{
	char			**tmp_env;
	unsigned int	cnt;
	int				ret;

	ret = 0;
	cnt = 0;
	if (env_flag(arg[cnt], &cnt, &tmp_env) != 0)
		return (1);
	while (arg[cnt] && ft_strchr(arg[cnt], '='))
	{
		add_tmp_env(tmp_env, arg[cnt]);
		cnt++;
	}
	if (arg[cnt])
		ret = ft_execute(&arg[cnt], tmp_env);
	else
		print_env(tmp_env);
	free_table(tmp_env);
	return (ret);
}
