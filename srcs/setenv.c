/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   setenv.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/08 15:27:15 by cumberto          #+#    #+#             */
/*   Updated: 2017/09/14 17:57:19 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		env_name_cmp(char *env, char *name)
{
	unsigned int ind;

	ind = 0;
	while (name[ind] && env[ind] == name[ind])
		ind++;
	if (name[ind] == '\0' && env[ind] == '=')
		return (0);
	return (1);
}

char	*find_pattern(char *pattern)
{
	extern char		**environ;
	unsigned int	cnt;

	cnt = 0;
	if (environ != NULL)
	{
		while (environ[cnt])
		{
			if (env_name_cmp(environ[cnt], pattern) == 0)
				return (ft_strchr(environ[cnt], '=') + 1);
			cnt++;
		}
	}
	return (NULL);
}

char	*set_env(char *name, char *value)
{
	extern char		**environ;
	unsigned int	cnt;
	char			*buf;

	cnt = 0;
	buf = ft_strnew(ft_strlen(name) + ft_strlen(value) + 1);
	buf = ft_strcat(ft_strcat(ft_strcat(buf, name), "="), value);
	while (environ[cnt])
	{
		if (env_name_cmp(environ[cnt], name) == 0)
		{
			free(environ[cnt]);
			environ[cnt] = buf;
			return (value);
		}
		cnt++;
	}
	environ[cnt] = buf;
	cnt++;
	environ[cnt] = NULL;
	return (value);
}

int		unset_env(char *name)
{
	extern char		**environ;
	char			**newenv;
	unsigned int	cnt;
	unsigned int	n_cnt;

	if (name == NULL)
		return (1);
	cnt = 0;
	n_cnt = 0;
	newenv = (char**)malloc(sizeof(char*) * 100);
	while (environ[cnt] != NULL)
	{
		if (env_name_cmp(environ[cnt], name))
		{
			newenv[n_cnt] = ft_strdup(environ[cnt]);
			n_cnt++;
		}
		cnt++;
	}
	newenv[n_cnt] = NULL;
	free_table(environ);
	environ = newenv;
	return (0);
}

int		env_mod(char **arg)
{
	if (arg[0] != NULL)
		if (arg[1] != NULL)
			if (arg[2] == NULL)
			{
				if (ft_strchr(arg[0], '=') == NULL)
				{
					set_env(arg[0], arg[1]);
					return (0);
				}
			}
	ft_dprintf(2, "setenv: wrong usage: [name] [value]\n", 37);
	return (1);
}
